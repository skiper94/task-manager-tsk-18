package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.model.Project;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll(final String userId){
        List<Project> list = new ArrayList<>();
        for (Project project : this.projects) {
            if (userId.equals(project.getUserId())) list.add(project);
        }
        return list;
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator, final String userId) {
        final List<Project> projects = new ArrayList<>(findAll(userId));
        projects.sort(comparator);
        return projects;
    }

    @Override
    public void add(final Project project, final String userId){
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final Project project, final String userId){
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @Override
    public void clear(final String userId){
        List<Project> list = findAll(userId);
        this.projects.removeAll(list);
    }

    @Override
    public Project findOneById(final String id, final String userId){
        for (final Project project: projects){
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String id, final String userId){
        final Project project = findOneById(id, userId);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project findOneByIndex(final Integer index, final String userId){
        List<Project> list = findAll(userId);
        return list.get(index);
    }

    @Override
    public Project findOneByName(final String name, final String userId){
        for (final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final Integer index, final String userId){
        final Project project = findOneByIndex(index, userId);
        if (project == null) return null;
        remove(project, userId);
        return project;
    }

    @Override
    public Project removeOneByName(final String name, final String userId){
        final Project project = findOneByName(name, userId);
        if (project == null) return null;
        remove(project, userId);
        return project;
    }

}
