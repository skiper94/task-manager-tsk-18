package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll(final String userId){
        List<Task> accessTasks = new ArrayList<>();
        for (Task task : this.tasks) {
            if (userId.equals(task.getUserId())) accessTasks.add(task);
        }
        return accessTasks;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator, final String userId) {
        final List<Task> tasksSort = new ArrayList<>(findAll(userId));
        tasksSort.sort(comparator);
        return tasksSort;
    }

    @Override
    public void add(final Task task, final String userId){
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final Task task, final String userId){
        if (!userId.equals(task.getUserId())) return;
        tasks.remove(task);
    }

    @Override
    public void clear(final String userId){
        List<Task> list = findAll(userId);
        this.tasks.removeAll(list);
    }

    @Override
    public Task findOneById(final String id, final String userId){
        for (final Task task: tasks){
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }

        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index, final String userId){
        List<Task> tasks = findAll(userId);
        return tasks.get(index);
    }

    @Override
    public Task findOneByName(final String name, final String userId){
        for (final Task task: tasks){
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id, final String userId){
        final Task task = findOneById(id, userId);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index, final String userId){
        final Task task = findOneByIndex(index, userId);
        if (task == null) return null;
        remove(task, userId);
        return task;
    }

    @Override
    public Task removeOneByName(final String name, final String userId){
        final Task task = findOneByName(name, userId);
        if (task == null) return null;
        remove(task, userId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId, final String userId) {
        List<Task> listOfTask = new ArrayList<>();
        for (Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) listOfTask.add(task);
        }
        return listOfTask;
    }

    @Override
    public List<Task> removeAllTaskByProjectId(final String projectId, final String userId) {
        final List<Task> listOfTask = findAllTaskByProjectId(projectId, userId);
        if (listOfTask == null) return null;
        for (Task task: listOfTask)
            tasks.remove(task);
        return tasks;
    }

    @Override
    public Task bindTaskByProject(final String projectId, final String taskId, final String userId) {
        final Task task = findOneById(taskId, userId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskByProjectId(final String taskId, final String userId) {
        final Task task = findOneById(taskId, userId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
