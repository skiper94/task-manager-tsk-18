package ru.apolyakov.tm.exception.system;

import ru.apolyakov.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Index is incorrect...");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Value \""+value+"\" is not number. Check value and repeat");
    }

}
