package ru.apolyakov.tm.exception.system;

import ru.apolyakov.tm.exception.AbstractException;

import java.util.Comparator;

public class ComparatorIncorrectException extends AbstractException {

    public ComparatorIncorrectException(){
        super("Error! Sort method is incorrect!");
    }

}
