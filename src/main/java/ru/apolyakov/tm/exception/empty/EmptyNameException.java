package ru.apolyakov.tm.exception.empty;

import ru.apolyakov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public  EmptyNameException() {
        super("Name is empty...");
    }

}
