package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // TASK CONTROLLER
    List<Task> findAllTaskByProjectId(String projectId, String userId);

    // TASK CONTROLLER
    Task bindTaskByProjectId(String projectId, String taskId, String userId);

    // TASK CONTROLLER
    Task unbindTaskFromProject(String projectId, String userId);

    // PROJECT CONTROLLER
    List<Task> removeTasksByProjectId(String projectId, String userId);

    Project removeProjectById(String projectId, String userId);
}
