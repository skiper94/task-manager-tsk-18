package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll(String userId);

    List<Task> findAll(Comparator<Task> comparator, String userId);

    Task add(String name, String description, String userId);

    void add(Task task, String userId);

    void remove(Task task, String userId);

    void clear(String userId);

    Task findOneById(String id, String userId);

    Task findOneByName(String name, String userId);

    Task findOneByIndex(Integer index, String userId);

    Task updateTaskById(String id, String name, String description, String userId);

    Task updateTaskByIndex(Integer index, String name, String description, String userId);

    Task removeOneByName(String name, String userId);

    Task removeOneById(String id, String userId);

    Task removeTaskByIndex(Integer index, String userId);

    Task startTaskById(String id, String userId);

    Task startTaskByIndex(Integer index, String userId);

    Task startTaskByName(String name, String userId);

    Task finishTaskById(String id, String userId);

    Task finishTaskByIndex(Integer index, String userId);

    Task finishTaskByName(String name, String userId);

    Task changeTaskStatusById(String id, Status status, String userId);

    Task changeTaskStatusByIndex(Integer index, Status status, String userId);

    Task changeTaskStatusByName(String name, Status status, String userId);

    Task updateTaskByName(String name, String nameNew, String description, String userId);

}
