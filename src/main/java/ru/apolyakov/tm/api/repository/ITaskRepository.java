package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll(String userId);

    List<Task> findAll(Comparator<Task> comparator, String userId);

    void add(Task task, String userId);

    void remove(Task task, String userId);

    void clear(String userId);

    List<Task> findAllTaskByProjectId(String projectId,String userId);

    List<Task> removeAllTaskByProjectId(String projectId,String userId);

    Task bindTaskByProject(String projectId, String taskId,String userId);

    Task unbindTaskByProjectId(String taskId,String userId);

    Task findOneById(String id,String userId);

    Task removeOneById(String id,String userId);

    Task findOneByIndex(Integer index,String userId);

    Task findOneByName(String name,String userId);

    Task removeOneByIndex(Integer index,String userId);

    Task removeOneByName(String name,String userId);


}
