package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll(String userId);


    List<Project> findAll(Comparator<Project> comparator, String userId);

    void add(Project project, String userId);

    void remove(Project project, String userId);

    void clear(String userId);

    Project findOneById(String id, String userId);

    Project removeOneById(String id, String userId);

    Project findOneByIndex(Integer index, String userId);

    Project findOneByName(String name, String userId);

    Project removeOneByIndex(Integer index, String userId);

    Project removeOneByName(String name, String userId);

}
