package ru.apolyakov.tm.service;

import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.api.service.IProjectService;
import ru.apolyakov.tm.exception.empty.EmptyDescriptionException;
import ru.apolyakov.tm.exception.empty.EmptyIdException;
import ru.apolyakov.tm.exception.empty.EmptyNameException;
import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.exception.system.ComparatorIncorrectException;
import ru.apolyakov.tm.exception.system.IndexIncorrectException;
import ru.apolyakov.tm.exception.system.StatusIncorrectException;
import ru.apolyakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;


    public ProjectService(final IProjectRepository projectRepository){
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll(final String userId) {
        return projectRepository.findAll(userId);
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator, final String userId) {
        if (comparator == null) throw new ComparatorIncorrectException();
        return projectRepository.findAll(comparator, userId);
    }

    @Override
    public Project add(final String name, final String description, final String userId){
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project, userId);
        return project;
    }

    @Override
    public void add(final Project project, final String userId) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project, userId);
    }

    @Override
    public void remove(final Project project, final String userId) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project, userId);
    }

    @Override
    public void clear(final String userId) {
        projectRepository.clear(userId);
    }

    @Override
    public Project removeOneByName(final String name, final String userId){
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(name, userId);
    }

    @Override
    public Project removeOneById(final String id, final String userId){
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(id, userId);
    }

    @Override
    public Project removeProjectByIndex(final Integer index, final String userId){
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.removeOneByIndex(index, userId);
    }

    @Override
    public Project findOneById(final String id, final String userId){
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(id, userId);
    }


    @Override
    public Project findOneByIndex(final Integer index, final String userId){
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index, userId);
    }


    @Override
    public Project findOneByName(final String name, final String userId){
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(name, userId);
    }


    @Override
    public Project updateProjectById(final String id, final String name, final String description, final String userId){
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByName(final String name, final String nameNew, final String description, final String userId){
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(name, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(nameNew);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description, final String userId){
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String id, final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index, final String userId) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String name, final String userId) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final String id, final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final Integer index, final String userId) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(final String name, final String userId) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status, final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new StatusIncorrectException();
        final Project project = findOneById(id, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status, final String userId) {
        if (index == null) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        final Project project = findOneByIndex(index, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final String name, final Status status, final String userId) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new StatusIncorrectException();
        final Project project = findOneByName(name, userId);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
