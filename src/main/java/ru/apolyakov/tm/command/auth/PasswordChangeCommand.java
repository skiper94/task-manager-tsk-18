package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.model.User;
import ru.apolyakov.tm.util.TerminalUtil;

public final class PasswordChangeCommand extends AbstractAuthCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "password-change";
    }

    @Override
    public String description() {
        return "Change password";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter new password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(user.getId(), password);
    }
}
